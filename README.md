# Place Health and Liveability: Intro to Git #
## How do I get set up? ##

First, you will want to have Git installed on your computer: https://git-scm.com/downloads

If running Windows, in file explorer when you right click on a folder you will see an option like 'Git Bash here'; this opens the Unix terminal window (a bit like windows cmd.exe) at that folder.  You can also use cmd.exe, but I found on the particular computer I use where I don't have admin rights this wouldn't allow me to browse down to C drive for some reason, whilst I could using Bash.  Anyway...here is how you start a repository of your own:

1. In windows cmd.exe or unix Bash move to directory where you want to store repository 
    * git clone https://**[username]**@bitbucket.org/jarozek/dummy-repository.git
        * creates an instance of this particular repository on your local computer
    * git init
        * starts your own new repository (not doing this just yet) 
2. Create a text file in the repository (in file explorer)
    * [yourname].txt
3. Tell git to track changes you make to the file
    * git add [yourname.txt]
        * works with all file types and folders in the repository
        * git add .
            * adds all folders in this dir  
4. Check the status
    * git status
        * should read:
            On branch master
            Your branch is up-to-date with 'origin/master'.
            Changes to be committed:
              (use "git reset HEAD <file>..." to unstage)
                    new file:   [yourname].txt
        * this means your new file hasn't been added to the online repository, it only exists in the local version
5. Make a comment about the changes you've made to the repository
    * git commit -m "Made a new text file"
        * this commits only the file(s) specified with "git add" in the previous step
     * git commit -a -m "[comment]"
        * this commits all files with changes located in the current repository (regardless of whether they were added or not)  	
6. Update the online repository with your local changes
    * git push
        * your new text file should now be listed in the bitbucket repository
7. Edit your text file, describe the changes and sync them online
    * git add [yourname].txt
    * Write something in the file
    * git commit -m "[Describe the changes you made]"
        * good comments are crucial- they are the whole point of git!
    * git add [yourname].txt
    * Make some more changes to the file
    * git commit -m "[Describe the changes you made]"
    * git push
    * This is the basic pattern of git- tell git to watch a file (add), make changes, describe those changes (commit), sync online (push)
8. Display history of commits / changes
    * git log --graph --oneline --all  
9. So far you've been tracking your own changes. The second major part of git is making sure everyone is up to date.
    * git pull
        * synchronises your local repository with the master shared version on Bitbucket
        * you should be able to see everyone elses [yourname].txt files in the local repository (in file explorer)
10. Partner up and edit each other's text file
    * repeat git add / commit / push / pull
    * view the changes 

1. Establish location of remote respository for this .git repo  (this could have been done at step 2, or any time)  
    * git remote add origin https://**[username]**@bitbucket.org/placehealthliveability/scripts.git	

## Using Bitbucket ##
### Organisation ###
This guide assumes you already have a Bitbucket account and have found this repository via an invitation.

The repositories you have access to are listed in a drop down menu above; this repository should appear as 'Intro to Git / dummy repository'.

There are two other drop down menus of note up the top:

* Teams
    * You can establish a team, however for the free Academic account these are limited to 5 members.  Team members share access to projects and repositories.  Given the limited capacity of teams, they may be better used for small group projects or core members.  However, repositories associated with teams can still be shared with others.
* Projects
    * You can situate repositories within projects

For the sake of demonstration, I have created this dummy repository within an 'Intro to Git' project for what I have called the PlaceHealthLiveability team (with the limitations described above). 

### Viewing contents of your repository ###
On the left hand side...

* click the link *Source*:  
    * This lists the contents of the repository (files and folders).  If you click on a text file, the contents of the currently active version (most likely, the latest) will be displayed. 

    * A drop down box above the file contents may be clicked on to view the version history.  This history of document versions committed to across time may be browsed. 

        ![dropdown.png](https://bitbucket.org/repo/d4kpxE/images/3380809319-dropdown.png)    

    * If you click 'Blame' you can view where changes were commited to by user and date, line by line.

        ![source_Blame.png](https://bitbucket.org/repo/d4kpxE/images/591921834-source_Blame.png)


* click the link *Commits*
    * This displays a log of changes to files in the repository commited over time, along with any comments you recorded to summarise the change.

        ![commits.png](https://bitbucket.org/repo/d4kpxE/images/1716628268-commits.png)

    * If you click on a particular commit here, you will be able to view modifications to text files (e.g. Python scripts, or Stata do files): additions are marked in green, and deletions in red.

        ![commit_changeLog.png](https://bitbucket.org/repo/d4kpxE/images/3100989675-commit_changeLog.png)

    * You can also view changes side by side with the previous version.

        ![commit_change_SideBySide.png](https://bitbucket.org/repo/d4kpxE/images/3334413808-commit_change_SideBySide.png)


## Other resources ##

* intro to Git by Software Carpentry
http://swcarpentry.github.io/git-novice/
* intro to Git on 'Data Analysis in Python' 
http://www.data-analysis-in-python.org/st_git_and_github.html
    * There is a lot of good stuff on this site � including introductions to using Python for GIS and network analysis, using ArcPy (for ArcGIS), and introductions to Python for R and Stata users)
http://www.data-analysis-in-python.org/index.html